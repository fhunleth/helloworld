# helloworld

This is a trivial autotools-based project.

## Building from source

First, clone this repository or
[download](https://bitbucket.org/fhunleth/helloworld/downloads/) an official
release.

If cloning, first run `./autgen.sh`. Building from an official release does not
require this step.

Then:

```
$ ./configure
$ make
$ make install
```

## Creating a release

Update `configure.ac` with the new version and run:
```
$ make dist
```

If it looks good, post it in the downloads section of BitBucket along with a
file containing the output of `sha256sum`.
